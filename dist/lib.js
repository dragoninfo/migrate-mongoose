'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

require('colors');

var _db = require('./db');

var _db2 = _interopRequireDefault(_db);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _inquirer = require('inquirer');

var _inquirer2 = _interopRequireDefault(_inquirer);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MigrationModel = void 0;

_bluebird2.default.config({
  warnings: false
});

var es6Template = '\n/**\n * Make any changes you need to make to the database here\n */\nexport async function up () {\n  // Write migration here\n}\n\n/**\n * Make any changes that UNDO the up function side effects here (if possible)\n */\nexport async function down () {\n  // Write migration here\n}\n';

var es5Template = '\'use strict\';\n\n/**\n * Make any changes you need to make to the database here\n */\nexports.up = function up (done) {\n  done();\n};\n\n/**\n * Make any changes that UNDO the up function side effects here (if possible)\n */\nexports.down = function down(done) {\n  done();\n};\n';

var Migrator = function () {
  function Migrator(_ref) {
    var templatePath = _ref.templatePath,
        _ref$migrationsPath = _ref.migrationsPath,
        migrationsPath = _ref$migrationsPath === undefined ? './migrations' : _ref$migrationsPath,
        dbConnectionUri = _ref.dbConnectionUri,
        _ref$es6Templates = _ref.es6Templates,
        es6Templates = _ref$es6Templates === undefined ? false : _ref$es6Templates,
        _ref$collectionName = _ref.collectionName,
        collectionName = _ref$collectionName === undefined ? 'migrations' : _ref$collectionName,
        _ref$autosync = _ref.autosync,
        autosync = _ref$autosync === undefined ? false : _ref$autosync,
        _ref$cli = _ref.cli,
        cli = _ref$cli === undefined ? false : _ref$cli,
        connection = _ref.connection;
    (0, _classCallCheck3.default)(this, Migrator);

    var defaultTemplate = es6Templates ? es6Template : es5Template;
    this.template = templatePath ? _fs2.default.readFileSync(templatePath, 'utf-8') : defaultTemplate;
    this.migrationPath = _path2.default.resolve(migrationsPath);
    this.connection = connection || _mongoose2.default.createConnection(dbConnectionUri, { useNewUrlParser: true });
    this.es6 = es6Templates;
    this.collection = collectionName;
    this.autosync = autosync;
    this.cli = cli;
    MigrationModel = (0, _db2.default)(collectionName, this.connection);
  }

  (0, _createClass3.default)(Migrator, [{
    key: 'log',
    value: function log(logString) {
      var force = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (force || this.cli) {
        console.log(logString);
      }
    }

    /**
     * Use your own Mongoose connection object (so you can use this('modelname')
     * @param {mongoose.connection} connection - Mongoose connection
     */

  }, {
    key: 'setMongooseConnection',
    value: function setMongooseConnection(connection) {
      MigrationModel = (0, _db2.default)(this.collection, connection);
    }

    /**
     * Close the underlying connection to mongo
     * @returns {Promise} A promise that resolves when connection is closed
     */

  }, {
    key: 'close',
    value: function close() {
      return this.connection ? this.connection.close() : _bluebird2.default.resolve();
    }

    /**
     * Create a new migration
     * @param {string} migrationName
     * @returns {Promise<Object>} A promise of the Migration created
     */

  }, {
    key: 'create',
    value: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(migrationName) {
        var existingMigration, now, newMigrationFile, migrationCreated;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return MigrationModel.findOne({
                  name: migrationName
                });

              case 3:
                existingMigration = _context.sent;

                if (!existingMigration) {
                  _context.next = 6;
                  break;
                }

                throw new Error(('There is already a migration with name \'' + migrationName + '\' in the database').red);

              case 6:
                _context.next = 8;
                return this.sync();

              case 8:
                now = Date.now();
                newMigrationFile = now + '-' + migrationName + '.js';

                _mkdirp2.default.sync(this.migrationPath);
                _fs2.default.writeFileSync(_path2.default.join(this.migrationPath, newMigrationFile), this.template);
                // create instance in db
                _context.next = 14;
                return this.connection;

              case 14:
                _context.next = 16;
                return MigrationModel.create({
                  name: migrationName,
                  createdAt: now
                });

              case 16:
                migrationCreated = _context.sent;

                this.log('Created migration ' + migrationName + ' in ' + this.migrationPath + '.');
                return _context.abrupt('return', migrationCreated);

              case 21:
                _context.prev = 21;
                _context.t0 = _context['catch'](0);

                this.log(_context.t0.stack);
                fileRequired(_context.t0);

              case 25:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 21]]);
      }));

      function create(_x2) {
        return _ref2.apply(this, arguments);
      }

      return create;
    }()

    /**
     * Runs migrations up to or down to a given migration name
     *
     * @param migrationName
     * @param direction
     */

  }, {
    key: 'run',
    value: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
        var _this = this;

        var direction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'up';
        var migrationName = arguments[1];

        var untilMigration, query, sortDirection, migrationsToRun, self, numMigrationsRan, migrationsRan, _loop, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, migration;

        return _regenerator2.default.wrap(function _callee2$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this.sync();

              case 2:
                if (!migrationName) {
                  _context3.next = 8;
                  break;
                }

                _context3.next = 5;
                return MigrationModel.findOne({
                  name: migrationName
                });

              case 5:
                _context3.t0 = _context3.sent;
                _context3.next = 11;
                break;

              case 8:
                _context3.next = 10;
                return MigrationModel.findOne().sort({
                  createdAt: -1
                });

              case 10:
                _context3.t0 = _context3.sent;

              case 11:
                untilMigration = _context3.t0;

                if (untilMigration) {
                  _context3.next = 18;
                  break;
                }

                if (!migrationName) {
                  _context3.next = 17;
                  break;
                }

                throw new ReferenceError("Could not find that migration in the database");

              case 17:
                throw new Error("There are no pending migrations.");

              case 18:
                query = {
                  createdAt: {
                    $lte: untilMigration.createdAt
                  },
                  state: 'down'
                };


                if (direction == 'down') {
                  query = {
                    createdAt: {
                      $gte: untilMigration.createdAt
                    },
                    state: 'up'
                  };
                }

                sortDirection = direction == 'up' ? 1 : -1;
                _context3.next = 23;
                return MigrationModel.find(query).sort({
                  createdAt: sortDirection
                });

              case 23:
                migrationsToRun = _context3.sent;

                if (migrationsToRun.length) {
                  _context3.next = 31;
                  break;
                }

                if (!this.cli) {
                  _context3.next = 30;
                  break;
                }

                this.log('There are no migrations to run'.yellow);
                this.log('Current Migrations\' Statuses: ');
                _context3.next = 30;
                return this.list();

              case 30:
                throw new Error('There are no migrations to run');

              case 31:
                self = this;
                numMigrationsRan = 0;
                migrationsRan = [];
                _loop = /*#__PURE__*/_regenerator2.default.mark(function _loop(migration) {
                  var migrationFilePath, migrationFunctions;
                  return _regenerator2.default.wrap(function _loop$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          migrationFilePath = _path2.default.join(self.migrationPath, migration.filename);

                          if (_this.es6) {
                            require('babel-register')({
                              "presets": [require("babel-preset-env"), require("babel-preset-stage-3")],
                              "plugins": [require("babel-plugin-transform-runtime"), require("babel-plugin-add-module-exports")]
                            });

                            require('babel-polyfill');
                          }

                          migrationFunctions = void 0;
                          _context2.prev = 3;

                          migrationFunctions = require(migrationFilePath);
                          _context2.next = 11;
                          break;

                        case 7:
                          _context2.prev = 7;
                          _context2.t0 = _context2['catch'](3);

                          _context2.t0.message = _context2.t0.message && /Unexpected token/.test(_context2.t0.message) ? 'Unexpected Token when parsing migration. If you are using an ES6 migration file, use option --es6' : _context2.t0.message;
                          throw _context2.t0;

                        case 11:
                          if (migrationFunctions[direction]) {
                            _context2.next = 13;
                            break;
                          }

                          throw new Error(('The ' + direction + ' export is not defined in ' + migration.filename + '.').red);

                        case 13:
                          _context2.prev = 13;
                          _context2.next = 16;
                          return new _bluebird2.default(function (resolve, reject) {
                            var callPromise = migrationFunctions[direction].call(_this.connection.model.bind(_this.connection), function callback(err) {
                              if (err) return reject(err);
                              resolve();
                            });

                            if (callPromise && typeof callPromise.then === 'function') {
                              callPromise.then(resolve).catch(reject);
                            }
                          });

                        case 16:

                          _this.log((direction.toUpperCase() + ':   ')[direction == 'up' ? 'green' : 'red'] + (' ' + migration.filename + ' '));

                          _context2.next = 19;
                          return MigrationModel.where({
                            name: migration.name
                          }).updateMany({
                            $set: {
                              state: direction
                            }
                          });

                        case 19:
                          migrationsRan.push(migration.toJSON());
                          numMigrationsRan++;
                          _context2.next = 28;
                          break;

                        case 23:
                          _context2.prev = 23;
                          _context2.t1 = _context2['catch'](13);

                          _this.log(('Failed to run migration ' + migration.name + ' due to an error.').red);
                          _this.log('Not continuing. Make sure your data is in consistent state'.red);
                          throw _context2.t1 instanceof Error ? _context2.t1 : new Error(_context2.t1);

                        case 28:
                        case 'end':
                          return _context2.stop();
                      }
                    }
                  }, _loop, _this, [[3, 7], [13, 23]]);
                });
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context3.prev = 38;
                _iterator = (0, _getIterator3.default)(migrationsToRun);

              case 40:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context3.next = 46;
                  break;
                }

                migration = _step.value;
                return _context3.delegateYield(_loop(migration), 't1', 43);

              case 43:
                _iteratorNormalCompletion = true;
                _context3.next = 40;
                break;

              case 46:
                _context3.next = 52;
                break;

              case 48:
                _context3.prev = 48;
                _context3.t2 = _context3['catch'](38);
                _didIteratorError = true;
                _iteratorError = _context3.t2;

              case 52:
                _context3.prev = 52;
                _context3.prev = 53;

                if (!_iteratorNormalCompletion && _iterator.return) {
                  _iterator.return();
                }

              case 55:
                _context3.prev = 55;

                if (!_didIteratorError) {
                  _context3.next = 58;
                  break;
                }

                throw _iteratorError;

              case 58:
                return _context3.finish(55);

              case 59:
                return _context3.finish(52);

              case 60:

                if (migrationsToRun.length == numMigrationsRan) this.log('All migrations finished successfully.'.green);
                return _context3.abrupt('return', migrationsRan);

              case 62:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee2, this, [[38, 48, 52, 60], [53,, 55, 59]]);
      }));

      function run() {
        return _ref3.apply(this, arguments);
      }

      return run;
    }()

    /**
     * Looks at the file system migrations and imports any migrations that are
     * on the file system but missing in the database into the database
     *
     * This functionality is opposite of prune()
     */

  }, {
    key: 'sync',
    value: function () {
      var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
        var _this2 = this;

        var filesInMigrationFolder, migrationsInDatabase, migrationsInFolder, filesNotInDb, migrationsToImport, answers;
        return _regenerator2.default.wrap(function _callee4$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                filesInMigrationFolder = _fs2.default.readdirSync(this.migrationPath);
                _context5.next = 4;
                return MigrationModel.find({});

              case 4:
                migrationsInDatabase = _context5.sent;

                // Go over migrations in folder and delete any files not in DB
                migrationsInFolder = _lodash2.default.filter(filesInMigrationFolder, function (file) {
                  return (/\d{13,}\-.+.js$/.test(file)
                  );
                }).map(function (filename) {
                  var fileCreatedAt = parseInt(filename.split('-')[0]);
                  var existsInDatabase = migrationsInDatabase.some(function (m) {
                    return filename == m.filename;
                  });
                  return {
                    createdAt: fileCreatedAt,
                    filename: filename,
                    existsInDatabase: existsInDatabase
                  };
                });
                filesNotInDb = _lodash2.default.filter(migrationsInFolder, {
                  existsInDatabase: false
                }).map(function (f) {
                  return f.filename;
                });
                migrationsToImport = filesNotInDb;

                this.log('Synchronizing database with file system migrations...');

                if (!(!this.autosync && migrationsToImport.length)) {
                  _context5.next = 14;
                  break;
                }

                _context5.next = 12;
                return new _bluebird2.default(function (resolve) {
                  _inquirer2.default.prompt({
                    type: 'checkbox',
                    message: 'The following migrations exist in the migrations folder but not in the database. Select the ones you want to import into the database',
                    name: 'migrationsToImport',
                    choices: filesNotInDb
                  }, function (answers) {
                    resolve(answers);
                  });
                });

              case 12:
                answers = _context5.sent;


                migrationsToImport = answers.migrationsToImport;

              case 14:
                return _context5.abrupt('return', _bluebird2.default.map(migrationsToImport, function () {
                  var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(migrationToImport) {
                    var filePath, timestampSeparatorIndex, timestamp, migrationName, createdMigration;
                    return _regenerator2.default.wrap(function _callee3$(_context4) {
                      while (1) {
                        switch (_context4.prev = _context4.next) {
                          case 0:
                            filePath = _path2.default.join(_this2.migrationPath, migrationToImport), timestampSeparatorIndex = migrationToImport.indexOf('-'), timestamp = migrationToImport.slice(0, timestampSeparatorIndex), migrationName = migrationToImport.slice(timestampSeparatorIndex + 1, migrationToImport.lastIndexOf('.'));


                            _this2.log('Adding migration ' + filePath + ' into database from file system. State is ' + 'DOWN'.red);
                            _context4.next = 4;
                            return MigrationModel.create({
                              name: migrationName,
                              createdAt: timestamp
                            });

                          case 4:
                            createdMigration = _context4.sent;
                            return _context4.abrupt('return', createdMigration.toJSON());

                          case 6:
                          case 'end':
                            return _context4.stop();
                        }
                      }
                    }, _callee3, _this2);
                  }));

                  return function (_x4) {
                    return _ref5.apply(this, arguments);
                  };
                }()));

              case 17:
                _context5.prev = 17;
                _context5.t0 = _context5['catch'](0);

                this.log('Could not synchronise migrations in the migrations folder up to the database.'.red);
                throw _context5.t0;

              case 21:
              case 'end':
                return _context5.stop();
            }
          }
        }, _callee4, this, [[0, 17]]);
      }));

      function sync() {
        return _ref4.apply(this, arguments);
      }

      return sync;
    }()

    /**
     * Opposite of sync().
     * Removes files in migration directory which don't exist in database.
     */

  }, {
    key: 'prune',
    value: function () {
      var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5() {
        var filesInMigrationFolder, migrationsInDatabase, migrationsInFolder, dbMigrationsNotOnFs, migrationsToDelete, answers, migrationsToDeleteDocs;
        return _regenerator2.default.wrap(function _callee5$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                filesInMigrationFolder = _fs2.default.readdirSync(this.migrationPath);
                _context6.next = 4;
                return MigrationModel.find({});

              case 4:
                migrationsInDatabase = _context6.sent;

                // Go over migrations in folder and delete any files not in DB
                migrationsInFolder = _lodash2.default.filter(filesInMigrationFolder, function (file) {
                  return (/\d{13,}\-.+.js/.test(file)
                  );
                }).map(function (filename) {
                  var fileCreatedAt = parseInt(filename.split('-')[0]);
                  var existsInDatabase = migrationsInDatabase.some(function (m) {
                    return filename == m.filename;
                  });
                  return {
                    createdAt: fileCreatedAt,
                    filename: filename,
                    existsInDatabase: existsInDatabase
                  };
                });
                dbMigrationsNotOnFs = _lodash2.default.filter(migrationsInDatabase, function (m) {
                  return !_lodash2.default.find(migrationsInFolder, {
                    filename: m.filename
                  });
                });
                migrationsToDelete = dbMigrationsNotOnFs.map(function (m) {
                  return m.name;
                });

                if (!(!this.autosync && !!migrationsToDelete.length)) {
                  _context6.next = 13;
                  break;
                }

                _context6.next = 11;
                return new _bluebird2.default(function (resolve) {
                  _inquirer2.default.prompt({
                    type: 'checkbox',
                    message: 'The following migrations exist in the database but not in the migrations folder. Select the ones you want to remove from the file system.',
                    name: 'migrationsToDelete',
                    choices: migrationsToDelete
                  }, function (answers) {
                    resolve(answers);
                  });
                });

              case 11:
                answers = _context6.sent;


                migrationsToDelete = answers.migrationsToDelete;

              case 13:
                _context6.next = 15;
                return MigrationModel.find({
                  name: {
                    $in: migrationsToDelete
                  }
                }).lean();

              case 15:
                migrationsToDeleteDocs = _context6.sent;

                if (!migrationsToDelete.length) {
                  _context6.next = 20;
                  break;
                }

                this.log('Removing migration(s) ', ('' + migrationsToDelete.join(', ')).cyan, ' from database');
                _context6.next = 20;
                return MigrationModel.remove({
                  name: {
                    $in: migrationsToDelete
                  }
                });

              case 20:
                return _context6.abrupt('return', migrationsToDeleteDocs);

              case 23:
                _context6.prev = 23;
                _context6.t0 = _context6['catch'](0);

                this.log('Could not prune extraneous migrations from database.'.red);
                throw _context6.t0;

              case 27:
              case 'end':
                return _context6.stop();
            }
          }
        }, _callee5, this, [[0, 23]]);
      }));

      function prune() {
        return _ref6.apply(this, arguments);
      }

      return prune;
    }()

    /**
     * Lists the current migrations and their statuses
     * @returns {Promise<Array<Object>>}
     * @example
     *   [
     *    { name: 'my-migration', filename: '149213223424_my-migration.js', state: 'up' },
     *    { name: 'add-cows', filename: '149213223453_add-cows.js', state: 'down' }
     *   ]
     */

  }, {
    key: 'list',
    value: function () {
      var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6() {
        var _this3 = this;

        var migrations;
        return _regenerator2.default.wrap(function _callee6$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return this.sync();

              case 2:
                _context7.next = 4;
                return MigrationModel.find().sort({
                  createdAt: 1
                });

              case 4:
                migrations = _context7.sent;

                if (!migrations.length) this.log('There are no migrations to list.'.yellow);
                return _context7.abrupt('return', migrations.map(function (m) {
                  _this3.log(('' + (m.state == 'up' ? 'UP:  \t' : 'DOWN:\t'))[m.state == 'up' ? 'green' : 'red'] + (' ' + m.filename));
                  return m.toJSON();
                }));

              case 7:
              case 'end':
                return _context7.stop();
            }
          }
        }, _callee6, this);
      }));

      function list() {
        return _ref7.apply(this, arguments);
      }

      return list;
    }()
  }]);
  return Migrator;
}();

exports.default = Migrator;


function fileRequired(error) {
  if (error && error.code == 'ENOENT') {
    throw new ReferenceError('Could not find any files at path \'' + error.path + '\'');
  }
}

module.exports = Migrator;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9saWIuanMiXSwibmFtZXMiOlsiTWlncmF0aW9uTW9kZWwiLCJQcm9taXNlIiwiY29uZmlnIiwid2FybmluZ3MiLCJlczZUZW1wbGF0ZSIsImVzNVRlbXBsYXRlIiwiTWlncmF0b3IiLCJ0ZW1wbGF0ZVBhdGgiLCJtaWdyYXRpb25zUGF0aCIsImRiQ29ubmVjdGlvblVyaSIsImVzNlRlbXBsYXRlcyIsImNvbGxlY3Rpb25OYW1lIiwiYXV0b3N5bmMiLCJjbGkiLCJjb25uZWN0aW9uIiwiZGVmYXVsdFRlbXBsYXRlIiwidGVtcGxhdGUiLCJmcyIsInJlYWRGaWxlU3luYyIsIm1pZ3JhdGlvblBhdGgiLCJwYXRoIiwicmVzb2x2ZSIsIm1vbmdvb3NlIiwiY3JlYXRlQ29ubmVjdGlvbiIsInVzZU5ld1VybFBhcnNlciIsImVzNiIsImNvbGxlY3Rpb24iLCJsb2dTdHJpbmciLCJmb3JjZSIsImNvbnNvbGUiLCJsb2ciLCJjbG9zZSIsIm1pZ3JhdGlvbk5hbWUiLCJmaW5kT25lIiwibmFtZSIsImV4aXN0aW5nTWlncmF0aW9uIiwiRXJyb3IiLCJyZWQiLCJzeW5jIiwibm93IiwiRGF0ZSIsIm5ld01pZ3JhdGlvbkZpbGUiLCJta2RpcnAiLCJ3cml0ZUZpbGVTeW5jIiwiam9pbiIsImNyZWF0ZSIsImNyZWF0ZWRBdCIsIm1pZ3JhdGlvbkNyZWF0ZWQiLCJzdGFjayIsImZpbGVSZXF1aXJlZCIsImRpcmVjdGlvbiIsInNvcnQiLCJ1bnRpbE1pZ3JhdGlvbiIsIlJlZmVyZW5jZUVycm9yIiwicXVlcnkiLCIkbHRlIiwic3RhdGUiLCIkZ3RlIiwic29ydERpcmVjdGlvbiIsImZpbmQiLCJtaWdyYXRpb25zVG9SdW4iLCJsZW5ndGgiLCJ5ZWxsb3ciLCJsaXN0Iiwic2VsZiIsIm51bU1pZ3JhdGlvbnNSYW4iLCJtaWdyYXRpb25zUmFuIiwibWlncmF0aW9uIiwibWlncmF0aW9uRmlsZVBhdGgiLCJmaWxlbmFtZSIsInJlcXVpcmUiLCJtaWdyYXRpb25GdW5jdGlvbnMiLCJtZXNzYWdlIiwidGVzdCIsInJlamVjdCIsImNhbGxQcm9taXNlIiwiY2FsbCIsIm1vZGVsIiwiYmluZCIsImNhbGxiYWNrIiwiZXJyIiwidGhlbiIsImNhdGNoIiwidG9VcHBlckNhc2UiLCJ3aGVyZSIsInVwZGF0ZU1hbnkiLCIkc2V0IiwicHVzaCIsInRvSlNPTiIsImdyZWVuIiwiZmlsZXNJbk1pZ3JhdGlvbkZvbGRlciIsInJlYWRkaXJTeW5jIiwibWlncmF0aW9uc0luRGF0YWJhc2UiLCJtaWdyYXRpb25zSW5Gb2xkZXIiLCJfIiwiZmlsdGVyIiwiZmlsZSIsIm1hcCIsImZpbGVDcmVhdGVkQXQiLCJwYXJzZUludCIsInNwbGl0IiwiZXhpc3RzSW5EYXRhYmFzZSIsInNvbWUiLCJtIiwiZmlsZXNOb3RJbkRiIiwiZiIsIm1pZ3JhdGlvbnNUb0ltcG9ydCIsImFzayIsInByb21wdCIsInR5cGUiLCJjaG9pY2VzIiwiYW5zd2VycyIsIm1pZ3JhdGlvblRvSW1wb3J0IiwiZmlsZVBhdGgiLCJ0aW1lc3RhbXBTZXBhcmF0b3JJbmRleCIsImluZGV4T2YiLCJ0aW1lc3RhbXAiLCJzbGljZSIsImxhc3RJbmRleE9mIiwiY3JlYXRlZE1pZ3JhdGlvbiIsImRiTWlncmF0aW9uc05vdE9uRnMiLCJtaWdyYXRpb25zVG9EZWxldGUiLCIkaW4iLCJsZWFuIiwibWlncmF0aW9uc1RvRGVsZXRlRG9jcyIsImN5YW4iLCJyZW1vdmUiLCJtaWdyYXRpb25zIiwiZXJyb3IiLCJjb2RlIiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQSxJQUFJQSx1QkFBSjs7QUFFQUMsbUJBQVFDLE1BQVIsQ0FBZTtBQUNiQyxZQUFVO0FBREcsQ0FBZjs7QUFJQSxJQUFNQyw4U0FBTjs7QUFpQkEsSUFBTUMsMFNBQU47O0lBbUJxQkMsUTtBQUNuQiwwQkFTRztBQUFBLFFBUkRDLFlBUUMsUUFSREEsWUFRQztBQUFBLG1DQVBEQyxjQU9DO0FBQUEsUUFQREEsY0FPQyx1Q0FQZ0IsY0FPaEI7QUFBQSxRQU5EQyxlQU1DLFFBTkRBLGVBTUM7QUFBQSxpQ0FMREMsWUFLQztBQUFBLFFBTERBLFlBS0MscUNBTGMsS0FLZDtBQUFBLG1DQUpEQyxjQUlDO0FBQUEsUUFKREEsY0FJQyx1Q0FKZ0IsWUFJaEI7QUFBQSw2QkFIREMsUUFHQztBQUFBLFFBSERBLFFBR0MsaUNBSFUsS0FHVjtBQUFBLHdCQUZEQyxHQUVDO0FBQUEsUUFGREEsR0FFQyw0QkFGSyxLQUVMO0FBQUEsUUFEREMsVUFDQyxRQUREQSxVQUNDO0FBQUE7O0FBQ0QsUUFBTUMsa0JBQWtCTCxlQUFlTixXQUFmLEdBQTZCQyxXQUFyRDtBQUNBLFNBQUtXLFFBQUwsR0FBZ0JULGVBQWVVLGFBQUdDLFlBQUgsQ0FBZ0JYLFlBQWhCLEVBQThCLE9BQTlCLENBQWYsR0FBd0RRLGVBQXhFO0FBQ0EsU0FBS0ksYUFBTCxHQUFxQkMsZUFBS0MsT0FBTCxDQUFhYixjQUFiLENBQXJCO0FBQ0EsU0FBS00sVUFBTCxHQUFrQkEsY0FBY1EsbUJBQVNDLGdCQUFULENBQTBCZCxlQUExQixFQUEyQyxFQUFFZSxpQkFBaUIsSUFBbkIsRUFBM0MsQ0FBaEM7QUFDQSxTQUFLQyxHQUFMLEdBQVdmLFlBQVg7QUFDQSxTQUFLZ0IsVUFBTCxHQUFrQmYsY0FBbEI7QUFDQSxTQUFLQyxRQUFMLEdBQWdCQSxRQUFoQjtBQUNBLFNBQUtDLEdBQUwsR0FBV0EsR0FBWDtBQUNBYixxQkFBaUIsa0JBQXNCVyxjQUF0QixFQUFzQyxLQUFLRyxVQUEzQyxDQUFqQjtBQUNEOzs7O3dCQUVHYSxTLEVBQTBCO0FBQUEsVUFBZkMsS0FBZSx1RUFBUCxLQUFPOztBQUM1QixVQUFJQSxTQUFTLEtBQUtmLEdBQWxCLEVBQXVCO0FBQ3JCZ0IsZ0JBQVFDLEdBQVIsQ0FBWUgsU0FBWjtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7Ozs7MENBSXNCYixVLEVBQVk7QUFDaENkLHVCQUFpQixrQkFBc0IsS0FBSzBCLFVBQTNCLEVBQXVDWixVQUF2QyxDQUFqQjtBQUNEOztBQUVEOzs7Ozs7OzRCQUlRO0FBQ04sYUFBTyxLQUFLQSxVQUFMLEdBQWtCLEtBQUtBLFVBQUwsQ0FBZ0JpQixLQUFoQixFQUFsQixHQUE0QzlCLG1CQUFRb0IsT0FBUixFQUFuRDtBQUNEOztBQUVEOzs7Ozs7Ozs7NEdBS2FXLGE7Ozs7Ozs7O3VCQUV1QmhDLGVBQWVpQyxPQUFmLENBQXVCO0FBQ3JEQyx3QkFBTUY7QUFEK0MsaUJBQXZCLEM7OztBQUExQkcsaUM7O29CQUdELENBQUNBLGlCOzs7OztzQkFDRSxJQUFJQyxLQUFKLENBQVUsK0NBQTJDSixhQUEzQyx5QkFBNEVLLEdBQXRGLEM7Ozs7dUJBR0YsS0FBS0MsSUFBTCxFOzs7QUFDQUMsbUIsR0FBTUMsS0FBS0QsR0FBTCxFO0FBQ05FLGdDLEdBQXNCRixHLFNBQU9QLGE7O0FBQ25DVSxpQ0FBT0osSUFBUCxDQUFZLEtBQUtuQixhQUFqQjtBQUNBRiw2QkFBRzBCLGFBQUgsQ0FBaUJ2QixlQUFLd0IsSUFBTCxDQUFVLEtBQUt6QixhQUFmLEVBQThCc0IsZ0JBQTlCLENBQWpCLEVBQWtFLEtBQUt6QixRQUF2RTtBQUNBOzt1QkFDTSxLQUFLRixVOzs7O3VCQUNvQmQsZUFBZTZDLE1BQWYsQ0FBc0I7QUFDbkRYLHdCQUFNRixhQUQ2QztBQUVuRGMsNkJBQVdQO0FBRndDLGlCQUF0QixDOzs7QUFBekJRLGdDOztBQUlOLHFCQUFLakIsR0FBTCx3QkFBOEJFLGFBQTlCLFlBQWtELEtBQUtiLGFBQXZEO2lEQUNPNEIsZ0I7Ozs7OztBQUVQLHFCQUFLakIsR0FBTCxDQUFTLFlBQU1rQixLQUFmO0FBQ0FDOzs7Ozs7Ozs7Ozs7Ozs7OztBQUlKOzs7Ozs7Ozs7Ozs7O1lBTVVDLFMsdUVBQVksSTtZQUFNbEIsYTs7Ozs7Ozs7O3VCQUNwQixLQUFLTSxJQUFMLEU7OztxQkFFaUJOLGE7Ozs7Ozt1QkFDZmhDLGVBQWVpQyxPQUFmLENBQXVCO0FBQzNCQyx3QkFBTUY7QUFEcUIsaUJBQXZCLEM7Ozs7Ozs7Ozt1QkFHQWhDLGVBQWVpQyxPQUFmLEdBQXlCa0IsSUFBekIsQ0FBOEI7QUFDbENMLDZCQUFXLENBQUM7QUFEc0IsaUJBQTlCLEM7Ozs7OztBQUpGTSw4Qjs7b0JBUURBLGM7Ozs7O3FCQUNDcEIsYTs7Ozs7c0JBQXFCLElBQUlxQixjQUFKLENBQW1CLCtDQUFuQixDOzs7c0JBQ2QsSUFBSWpCLEtBQUosQ0FBVSxrQ0FBVixDOzs7QUFHVGtCLHFCLEdBQVE7QUFDVlIsNkJBQVc7QUFDVFMsMEJBQU1ILGVBQWVOO0FBRFosbUJBREQ7QUFJVlUseUJBQU87QUFKRyxpQjs7O0FBT1osb0JBQUlOLGFBQWEsTUFBakIsRUFBeUI7QUFDdkJJLDBCQUFRO0FBQ05SLCtCQUFXO0FBQ1RXLDRCQUFNTCxlQUFlTjtBQURaLHFCQURMO0FBSU5VLDJCQUFPO0FBSkQsbUJBQVI7QUFNRDs7QUFHS0UsNkIsR0FBZ0JSLGFBQWEsSUFBYixHQUFvQixDQUFwQixHQUF3QixDQUFDLEM7O3VCQUNqQmxELGVBQWUyRCxJQUFmLENBQW9CTCxLQUFwQixFQUMzQkgsSUFEMkIsQ0FDdEI7QUFDSkwsNkJBQVdZO0FBRFAsaUJBRHNCLEM7OztBQUF4QkUsK0I7O29CQUtEQSxnQkFBZ0JDLE07Ozs7O3FCQUNmLEtBQUtoRCxHOzs7OztBQUNQLHFCQUFLaUIsR0FBTCxDQUFTLGlDQUFpQ2dDLE1BQTFDO0FBQ0EscUJBQUtoQyxHQUFMOzt1QkFDTSxLQUFLaUMsSUFBTCxFOzs7c0JBRUYsSUFBSTNCLEtBQUosQ0FBVSxnQ0FBVixDOzs7QUFHSjRCLG9CLEdBQU8sSTtBQUNQQyxnQyxHQUFtQixDO0FBQ25CQyw2QixHQUFnQixFOytFQUVUQyxTOzs7Ozs7QUFDSEMsMkMsR0FBb0JoRCxlQUFLd0IsSUFBTCxDQUFVb0IsS0FBSzdDLGFBQWYsRUFBOEJnRCxVQUFVRSxRQUF4QyxDOztBQUMxQiw4QkFBSSxNQUFLNUMsR0FBVCxFQUFjO0FBQ1o2QyxvQ0FBUSxnQkFBUixFQUEwQjtBQUN4Qix5Q0FBVyxDQUFDQSxRQUFRLGtCQUFSLENBQUQsRUFBOEJBLFFBQVEsc0JBQVIsQ0FBOUIsQ0FEYTtBQUV4Qix5Q0FBVyxDQUFDQSxRQUFRLGdDQUFSLENBQUQsRUFBNENBLFFBQVEsaUNBQVIsQ0FBNUM7QUFGYSw2QkFBMUI7O0FBS0FBLG9DQUFRLGdCQUFSO0FBQ0Q7O0FBRUdDLDRDOzs7QUFHRkEsK0NBQXFCRCxRQUFRRixpQkFBUixDQUFyQjs7Ozs7Ozs7QUFFQSx1Q0FBSUksT0FBSixHQUFjLGFBQUlBLE9BQUosSUFBZSxtQkFBbUJDLElBQW5CLENBQXdCLGFBQUlELE9BQTVCLENBQWYsR0FDWixtR0FEWSxHQUVaLGFBQUlBLE9BRk47Ozs7OEJBTUdELG1CQUFtQnJCLFNBQW5CLEM7Ozs7O2dDQUNHLElBQUlkLEtBQUosQ0FBVSxVQUFPYyxTQUFQLGtDQUE2Q2lCLFVBQVVFLFFBQXZELFFBQW1FaEMsR0FBN0UsQzs7Ozs7aUNBSUEsSUFBSXBDLGtCQUFKLENBQVksVUFBQ29CLE9BQUQsRUFBVXFELE1BQVYsRUFBcUI7QUFDckMsZ0NBQU1DLGNBQWNKLG1CQUFtQnJCLFNBQW5CLEVBQThCMEIsSUFBOUIsQ0FDbEIsTUFBSzlELFVBQUwsQ0FBZ0IrRCxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkIsTUFBS2hFLFVBQWhDLENBRGtCLEVBRWxCLFNBQVNpRSxRQUFULENBQWtCQyxHQUFsQixFQUF1QjtBQUNyQixrQ0FBSUEsR0FBSixFQUFTLE9BQU9OLE9BQU9NLEdBQVAsQ0FBUDtBQUNUM0Q7QUFDRCw2QkFMaUIsQ0FBcEI7O0FBUUEsZ0NBQUlzRCxlQUFlLE9BQU9BLFlBQVlNLElBQW5CLEtBQTRCLFVBQS9DLEVBQTJEO0FBQ3pETiwwQ0FBWU0sSUFBWixDQUFpQjVELE9BQWpCLEVBQTBCNkQsS0FBMUIsQ0FBZ0NSLE1BQWhDO0FBQ0Q7QUFDRiwyQkFaSyxDOzs7O0FBY04sZ0NBQUs1QyxHQUFMLENBQVMsQ0FBR29CLFVBQVVpQyxXQUFWLEVBQUgsV0FBa0NqQyxhQUFhLElBQWIsR0FBb0IsT0FBcEIsR0FBOEIsS0FBaEUsV0FBNkVpQixVQUFVRSxRQUF2RixPQUFUOzs7aUNBRU1yRSxlQUFlb0YsS0FBZixDQUFxQjtBQUN6QmxELGtDQUFNaUMsVUFBVWpDO0FBRFMsMkJBQXJCLEVBRUhtRCxVQUZHLENBRVE7QUFDWkMsa0NBQU07QUFDSjlCLHFDQUFPTjtBQURIO0FBRE0sMkJBRlIsQzs7O0FBT05nQix3Q0FBY3FCLElBQWQsQ0FBbUJwQixVQUFVcUIsTUFBVixFQUFuQjtBQUNBdkI7Ozs7Ozs7O0FBRUEsZ0NBQUtuQyxHQUFMLENBQVMsOEJBQTJCcUMsVUFBVWpDLElBQXJDLHdCQUE2REcsR0FBdEU7QUFDQSxnQ0FBS1AsR0FBTCxDQUFTLDZEQUE2RE8sR0FBdEU7Z0NBQ00sd0JBQWVELEtBQWYsa0JBQThCLElBQUlBLEtBQUosYzs7Ozs7Ozs7Ozs7Ozt1REF2RGhCd0IsZTs7Ozs7Ozs7QUFBYk8seUI7cURBQUFBLFM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyRFgsb0JBQUlQLGdCQUFnQkMsTUFBaEIsSUFBMEJJLGdCQUE5QixFQUFnRCxLQUFLbkMsR0FBTCxDQUFTLHdDQUF3QzJELEtBQWpEO2tEQUN6Q3ZCLGE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR1Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFRVXdCLHNDLEdBQXlCekUsYUFBRzBFLFdBQUgsQ0FBZSxLQUFLeEUsYUFBcEIsQzs7dUJBQ0luQixlQUFlMkQsSUFBZixDQUFvQixFQUFwQixDOzs7QUFBN0JpQyxvQzs7QUFDTjtBQUNNQyxrQyxHQUFxQkMsaUJBQUVDLE1BQUYsQ0FBU0wsc0JBQVQsRUFBaUM7QUFBQSx5QkFBUSxtQkFBa0JqQixJQUFsQixDQUF1QnVCLElBQXZCO0FBQVI7QUFBQSxpQkFBakMsRUFDeEJDLEdBRHdCLENBQ3BCLG9CQUFZO0FBQ2Ysc0JBQU1DLGdCQUFnQkMsU0FBUzlCLFNBQVMrQixLQUFULENBQWUsR0FBZixFQUFvQixDQUFwQixDQUFULENBQXRCO0FBQ0Esc0JBQU1DLG1CQUFtQlQscUJBQXFCVSxJQUFyQixDQUEwQjtBQUFBLDJCQUFLakMsWUFBWWtDLEVBQUVsQyxRQUFuQjtBQUFBLG1CQUExQixDQUF6QjtBQUNBLHlCQUFPO0FBQ0x2QiwrQkFBV29ELGFBRE47QUFFTDdCLHNDQUZLO0FBR0xnQztBQUhLLG1CQUFQO0FBS0QsaUJBVHdCLEM7QUFXckJHLDRCLEdBQWVWLGlCQUFFQyxNQUFGLENBQVNGLGtCQUFULEVBQTZCO0FBQ2hEUSxvQ0FBa0I7QUFEOEIsaUJBQTdCLEVBRWxCSixHQUZrQixDQUVkO0FBQUEseUJBQUtRLEVBQUVwQyxRQUFQO0FBQUEsaUJBRmMsQztBQUdqQnFDLGtDLEdBQXFCRixZOztBQUN6QixxQkFBSzFFLEdBQUwsQ0FBUyx1REFBVDs7c0JBQ0ksQ0FBQyxLQUFLbEIsUUFBTixJQUFrQjhGLG1CQUFtQjdDLE07Ozs7Ozt1QkFDakIsSUFBSTVELGtCQUFKLENBQVksVUFBU29CLE9BQVQsRUFBa0I7QUFDbERzRixxQ0FBSUMsTUFBSixDQUFXO0FBQ1RDLDBCQUFNLFVBREc7QUFFVHJDLDZCQUFTLHVJQUZBO0FBR1R0QywwQkFBTSxvQkFIRztBQUlUNEUsNkJBQVNOO0FBSkEsbUJBQVgsRUFLRyxVQUFDTyxPQUFELEVBQWE7QUFDZDFGLDRCQUFRMEYsT0FBUjtBQUNELG1CQVBEO0FBUUQsaUJBVHFCLEM7OztBQUFoQkEsdUI7OztBQVdOTCxxQ0FBcUJLLFFBQVFMLGtCQUE3Qjs7O2tEQUdLekcsbUJBQVFnRyxHQUFSLENBQVlTLGtCQUFaO0FBQUEsdUdBQWdDLGtCQUFPTSxpQkFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDL0JDLG9DQUQrQixHQUNwQjdGLGVBQUt3QixJQUFMLENBQVUsT0FBS3pCLGFBQWYsRUFBOEI2RixpQkFBOUIsQ0FEb0IsRUFFbkNFLHVCQUZtQyxHQUVURixrQkFBa0JHLE9BQWxCLENBQTBCLEdBQTFCLENBRlMsRUFHbkNDLFNBSG1DLEdBR3ZCSixrQkFBa0JLLEtBQWxCLENBQXdCLENBQXhCLEVBQTJCSCx1QkFBM0IsQ0FIdUIsRUFJbkNsRixhQUptQyxHQUluQmdGLGtCQUFrQkssS0FBbEIsQ0FBd0JILDBCQUEwQixDQUFsRCxFQUFxREYsa0JBQWtCTSxXQUFsQixDQUE4QixHQUE5QixDQUFyRCxDQUptQjs7O0FBTXJDLG1DQUFLeEYsR0FBTCxDQUFTLHNCQUFvQm1GLFFBQXBCLGtEQUEyRSxPQUFPNUUsR0FBM0Y7QUFOcUM7QUFBQSxtQ0FPTnJDLGVBQWU2QyxNQUFmLENBQXNCO0FBQ25EWCxvQ0FBTUYsYUFENkM7QUFFbkRjLHlDQUFXc0U7QUFGd0MsNkJBQXRCLENBUE07O0FBQUE7QUFPL0JHLDRDQVArQjtBQUFBLDhEQVc5QkEsaUJBQWlCL0IsTUFBakIsRUFYOEI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQWhDOztBQUFBO0FBQUE7QUFBQTtBQUFBLG9COzs7Ozs7QUFjUCxxQkFBSzFELEdBQUwsQ0FBUyxnRkFBZ0ZPLEdBQXpGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLSjs7Ozs7Ozs7Ozs7Ozs7O0FBTVVxRCxzQyxHQUF5QnpFLGFBQUcwRSxXQUFILENBQWUsS0FBS3hFLGFBQXBCLEM7O3VCQUNJbkIsZUFBZTJELElBQWYsQ0FBb0IsRUFBcEIsQzs7O0FBQTdCaUMsb0M7O0FBQ047QUFDTUMsa0MsR0FBcUJDLGlCQUFFQyxNQUFGLENBQVNMLHNCQUFULEVBQWlDO0FBQUEseUJBQVEsa0JBQWlCakIsSUFBakIsQ0FBc0J1QixJQUF0QjtBQUFSO0FBQUEsaUJBQWpDLEVBQ3hCQyxHQUR3QixDQUNwQixvQkFBWTtBQUNmLHNCQUFNQyxnQkFBZ0JDLFNBQVM5QixTQUFTK0IsS0FBVCxDQUFlLEdBQWYsRUFBb0IsQ0FBcEIsQ0FBVCxDQUF0QjtBQUNBLHNCQUFNQyxtQkFBbUJULHFCQUFxQlUsSUFBckIsQ0FBMEI7QUFBQSwyQkFBS2pDLFlBQVlrQyxFQUFFbEMsUUFBbkI7QUFBQSxtQkFBMUIsQ0FBekI7QUFDQSx5QkFBTztBQUNMdkIsK0JBQVdvRCxhQUROO0FBRUw3QixzQ0FGSztBQUdMZ0M7QUFISyxtQkFBUDtBQUtELGlCQVR3QixDO0FBV3JCbUIsbUMsR0FBc0IxQixpQkFBRUMsTUFBRixDQUFTSCxvQkFBVCxFQUErQixhQUFLO0FBQzlELHlCQUFPLENBQUNFLGlCQUFFbkMsSUFBRixDQUFPa0Msa0JBQVAsRUFBMkI7QUFDakN4Qiw4QkFBVWtDLEVBQUVsQztBQURxQixtQkFBM0IsQ0FBUjtBQUdELGlCQUoyQixDO0FBT3hCb0Qsa0MsR0FBcUJELG9CQUFvQnZCLEdBQXBCLENBQXdCO0FBQUEseUJBQUtNLEVBQUVyRSxJQUFQO0FBQUEsaUJBQXhCLEM7O3NCQUVyQixDQUFDLEtBQUt0QixRQUFOLElBQWtCLENBQUMsQ0FBQzZHLG1CQUFtQjVELE07Ozs7Ozt1QkFDbkIsSUFBSTVELGtCQUFKLENBQVksVUFBU29CLE9BQVQsRUFBa0I7QUFDbERzRixxQ0FBSUMsTUFBSixDQUFXO0FBQ1RDLDBCQUFNLFVBREc7QUFFVHJDLDZCQUFTLDJJQUZBO0FBR1R0QywwQkFBTSxvQkFIRztBQUlUNEUsNkJBQVNXO0FBSkEsbUJBQVgsRUFLRyxVQUFDVixPQUFELEVBQWE7QUFDZDFGLDRCQUFRMEYsT0FBUjtBQUNELG1CQVBEO0FBUUQsaUJBVHFCLEM7OztBQUFoQkEsdUI7OztBQVdOVSxxQ0FBcUJWLFFBQVFVLGtCQUE3Qjs7Ozt1QkFHbUN6SCxlQUNsQzJELElBRGtDLENBQzdCO0FBQ0p6Qix3QkFBTTtBQUNKd0YseUJBQUtEO0FBREQ7QUFERixpQkFENkIsRUFLaENFLElBTGdDLEU7OztBQUEvQkMsc0M7O3FCQU9GSCxtQkFBbUI1RCxNOzs7OztBQUNyQixxQkFBSy9CLEdBQUwsMkJBQW1DLE1BQUcyRixtQkFBbUI3RSxJQUFuQixDQUF3QixJQUF4QixDQUFILEVBQW1DaUYsSUFBdEU7O3VCQUNNN0gsZUFBZThILE1BQWYsQ0FBc0I7QUFDMUI1Rix3QkFBTTtBQUNKd0YseUJBQUtEO0FBREQ7QUFEb0IsaUJBQXRCLEM7OztrREFPREcsc0I7Ozs7OztBQUVQLHFCQUFLOUYsR0FBTCxDQUFTLHVEQUF1RE8sR0FBaEU7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUtKOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3VCQVVRLEtBQUtDLElBQUwsRTs7Ozt1QkFDbUJ0QyxlQUFlMkQsSUFBZixHQUFzQlIsSUFBdEIsQ0FBMkI7QUFDbERMLDZCQUFXO0FBRHVDLGlCQUEzQixDOzs7QUFBbkJpRiwwQjs7QUFHTixvQkFBSSxDQUFDQSxXQUFXbEUsTUFBaEIsRUFBd0IsS0FBSy9CLEdBQUwsQ0FBUyxtQ0FBbUNnQyxNQUE1QztrREFDakJpRSxXQUFXOUIsR0FBWCxDQUFlLFVBQUNNLENBQUQsRUFBTztBQUMzQix5QkFBS3pFLEdBQUwsQ0FDRSxPQUFHeUUsRUFBRS9DLEtBQUYsSUFBVyxJQUFYLEdBQWtCLFNBQWxCLEdBQThCLFNBQWpDLEdBQThDK0MsRUFBRS9DLEtBQUYsSUFBVyxJQUFYLEdBQWtCLE9BQWxCLEdBQTRCLEtBQTFFLFdBQ0krQyxFQUFFbEMsUUFETixDQURGO0FBSUEseUJBQU9rQyxFQUFFZixNQUFGLEVBQVA7QUFDRCxpQkFOTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztrQkFyVlVsRixROzs7QUFpV3JCLFNBQVMyQyxZQUFULENBQXNCK0UsS0FBdEIsRUFBNkI7QUFDM0IsTUFBSUEsU0FBU0EsTUFBTUMsSUFBTixJQUFjLFFBQTNCLEVBQXFDO0FBQ25DLFVBQU0sSUFBSTVFLGNBQUoseUNBQXdEMkUsTUFBTTVHLElBQTlELFFBQU47QUFDRDtBQUNGOztBQUdEOEcsT0FBT0MsT0FBUCxHQUFpQjdILFFBQWpCIiwiZmlsZSI6ImxpYi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAnY29sb3JzJztcblxuaW1wb3J0IE1pZ3JhdGlvbk1vZGVsRmFjdG9yeSBmcm9tICcuL2RiJztcbmltcG9ydCBQcm9taXNlIGZyb20gJ2JsdWViaXJkJztcbmltcG9ydCBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgYXNrIGZyb20gJ2lucXVpcmVyJztcbmltcG9ydCBmcyBmcm9tICdmcyc7XG5pbXBvcnQgbWtkaXJwIGZyb20gJ21rZGlycCc7XG5pbXBvcnQgbW9uZ29vc2UgZnJvbSAnbW9uZ29vc2UnO1xuaW1wb3J0IHBhdGggZnJvbSAncGF0aCc7XG5cbmxldCBNaWdyYXRpb25Nb2RlbDtcblxuUHJvbWlzZS5jb25maWcoe1xuICB3YXJuaW5nczogZmFsc2Vcbn0pO1xuXG5jb25zdCBlczZUZW1wbGF0ZSA9XG4gIGBcbi8qKlxuICogTWFrZSBhbnkgY2hhbmdlcyB5b3UgbmVlZCB0byBtYWtlIHRvIHRoZSBkYXRhYmFzZSBoZXJlXG4gKi9cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiB1cCAoKSB7XG4gIC8vIFdyaXRlIG1pZ3JhdGlvbiBoZXJlXG59XG5cbi8qKlxuICogTWFrZSBhbnkgY2hhbmdlcyB0aGF0IFVORE8gdGhlIHVwIGZ1bmN0aW9uIHNpZGUgZWZmZWN0cyBoZXJlIChpZiBwb3NzaWJsZSlcbiAqL1xuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGRvd24gKCkge1xuICAvLyBXcml0ZSBtaWdyYXRpb24gaGVyZVxufVxuYDtcblxuY29uc3QgZXM1VGVtcGxhdGUgPVxuICBgJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIE1ha2UgYW55IGNoYW5nZXMgeW91IG5lZWQgdG8gbWFrZSB0byB0aGUgZGF0YWJhc2UgaGVyZVxuICovXG5leHBvcnRzLnVwID0gZnVuY3Rpb24gdXAgKGRvbmUpIHtcbiAgZG9uZSgpO1xufTtcblxuLyoqXG4gKiBNYWtlIGFueSBjaGFuZ2VzIHRoYXQgVU5ETyB0aGUgdXAgZnVuY3Rpb24gc2lkZSBlZmZlY3RzIGhlcmUgKGlmIHBvc3NpYmxlKVxuICovXG5leHBvcnRzLmRvd24gPSBmdW5jdGlvbiBkb3duKGRvbmUpIHtcbiAgZG9uZSgpO1xufTtcbmA7XG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTWlncmF0b3Ige1xuICBjb25zdHJ1Y3Rvcih7XG4gICAgdGVtcGxhdGVQYXRoLFxuICAgIG1pZ3JhdGlvbnNQYXRoID0gJy4vbWlncmF0aW9ucycsXG4gICAgZGJDb25uZWN0aW9uVXJpLFxuICAgIGVzNlRlbXBsYXRlcyA9IGZhbHNlLFxuICAgIGNvbGxlY3Rpb25OYW1lID0gJ21pZ3JhdGlvbnMnLFxuICAgIGF1dG9zeW5jID0gZmFsc2UsXG4gICAgY2xpID0gZmFsc2UsXG4gICAgY29ubmVjdGlvblxuICB9KSB7XG4gICAgY29uc3QgZGVmYXVsdFRlbXBsYXRlID0gZXM2VGVtcGxhdGVzID8gZXM2VGVtcGxhdGUgOiBlczVUZW1wbGF0ZTtcbiAgICB0aGlzLnRlbXBsYXRlID0gdGVtcGxhdGVQYXRoID8gZnMucmVhZEZpbGVTeW5jKHRlbXBsYXRlUGF0aCwgJ3V0Zi04JykgOiBkZWZhdWx0VGVtcGxhdGU7XG4gICAgdGhpcy5taWdyYXRpb25QYXRoID0gcGF0aC5yZXNvbHZlKG1pZ3JhdGlvbnNQYXRoKTtcbiAgICB0aGlzLmNvbm5lY3Rpb24gPSBjb25uZWN0aW9uIHx8IG1vbmdvb3NlLmNyZWF0ZUNvbm5lY3Rpb24oZGJDb25uZWN0aW9uVXJpLCB7IHVzZU5ld1VybFBhcnNlcjogdHJ1ZSB9KTtcbiAgICB0aGlzLmVzNiA9IGVzNlRlbXBsYXRlcztcbiAgICB0aGlzLmNvbGxlY3Rpb24gPSBjb2xsZWN0aW9uTmFtZTtcbiAgICB0aGlzLmF1dG9zeW5jID0gYXV0b3N5bmM7XG4gICAgdGhpcy5jbGkgPSBjbGk7XG4gICAgTWlncmF0aW9uTW9kZWwgPSBNaWdyYXRpb25Nb2RlbEZhY3RvcnkoY29sbGVjdGlvbk5hbWUsIHRoaXMuY29ubmVjdGlvbik7XG4gIH1cblxuICBsb2cobG9nU3RyaW5nLCBmb3JjZSA9IGZhbHNlKSB7XG4gICAgaWYgKGZvcmNlIHx8IHRoaXMuY2xpKSB7XG4gICAgICBjb25zb2xlLmxvZyhsb2dTdHJpbmcpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBVc2UgeW91ciBvd24gTW9uZ29vc2UgY29ubmVjdGlvbiBvYmplY3QgKHNvIHlvdSBjYW4gdXNlIHRoaXMoJ21vZGVsbmFtZScpXG4gICAqIEBwYXJhbSB7bW9uZ29vc2UuY29ubmVjdGlvbn0gY29ubmVjdGlvbiAtIE1vbmdvb3NlIGNvbm5lY3Rpb25cbiAgICovXG4gIHNldE1vbmdvb3NlQ29ubmVjdGlvbihjb25uZWN0aW9uKSB7XG4gICAgTWlncmF0aW9uTW9kZWwgPSBNaWdyYXRpb25Nb2RlbEZhY3RvcnkodGhpcy5jb2xsZWN0aW9uLCBjb25uZWN0aW9uKVxuICB9XG5cbiAgLyoqXG4gICAqIENsb3NlIHRoZSB1bmRlcmx5aW5nIGNvbm5lY3Rpb24gdG8gbW9uZ29cbiAgICogQHJldHVybnMge1Byb21pc2V9IEEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdoZW4gY29ubmVjdGlvbiBpcyBjbG9zZWRcbiAgICovXG4gIGNsb3NlKCkge1xuICAgIHJldHVybiB0aGlzLmNvbm5lY3Rpb24gPyB0aGlzLmNvbm5lY3Rpb24uY2xvc2UoKSA6IFByb21pc2UucmVzb2x2ZSgpO1xuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZSBhIG5ldyBtaWdyYXRpb25cbiAgICogQHBhcmFtIHtzdHJpbmd9IG1pZ3JhdGlvbk5hbWVcbiAgICogQHJldHVybnMge1Byb21pc2U8T2JqZWN0Pn0gQSBwcm9taXNlIG9mIHRoZSBNaWdyYXRpb24gY3JlYXRlZFxuICAgKi9cbiAgYXN5bmMgY3JlYXRlKG1pZ3JhdGlvbk5hbWUpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgZXhpc3RpbmdNaWdyYXRpb24gPSBhd2FpdCBNaWdyYXRpb25Nb2RlbC5maW5kT25lKHtcbiAgICAgICAgbmFtZTogbWlncmF0aW9uTmFtZVxuICAgICAgfSk7XG4gICAgICBpZiAoISFleGlzdGluZ01pZ3JhdGlvbikge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYFRoZXJlIGlzIGFscmVhZHkgYSBtaWdyYXRpb24gd2l0aCBuYW1lICcke21pZ3JhdGlvbk5hbWV9JyBpbiB0aGUgZGF0YWJhc2VgLnJlZCk7XG4gICAgICB9XG5cbiAgICAgIGF3YWl0IHRoaXMuc3luYygpO1xuICAgICAgY29uc3Qgbm93ID0gRGF0ZS5ub3coKTtcbiAgICAgIGNvbnN0IG5ld01pZ3JhdGlvbkZpbGUgPSBgJHtub3d9LSR7bWlncmF0aW9uTmFtZX0uanNgO1xuICAgICAgbWtkaXJwLnN5bmModGhpcy5taWdyYXRpb25QYXRoKTtcbiAgICAgIGZzLndyaXRlRmlsZVN5bmMocGF0aC5qb2luKHRoaXMubWlncmF0aW9uUGF0aCwgbmV3TWlncmF0aW9uRmlsZSksIHRoaXMudGVtcGxhdGUpO1xuICAgICAgLy8gY3JlYXRlIGluc3RhbmNlIGluIGRiXG4gICAgICBhd2FpdCB0aGlzLmNvbm5lY3Rpb247XG4gICAgICBjb25zdCBtaWdyYXRpb25DcmVhdGVkID0gYXdhaXQgTWlncmF0aW9uTW9kZWwuY3JlYXRlKHtcbiAgICAgICAgbmFtZTogbWlncmF0aW9uTmFtZSxcbiAgICAgICAgY3JlYXRlZEF0OiBub3dcbiAgICAgIH0pO1xuICAgICAgdGhpcy5sb2coYENyZWF0ZWQgbWlncmF0aW9uICR7bWlncmF0aW9uTmFtZX0gaW4gJHt0aGlzLm1pZ3JhdGlvblBhdGh9LmApO1xuICAgICAgcmV0dXJuIG1pZ3JhdGlvbkNyZWF0ZWQ7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIHRoaXMubG9nKGVycm9yLnN0YWNrKTtcbiAgICAgIGZpbGVSZXF1aXJlZChlcnJvcik7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFJ1bnMgbWlncmF0aW9ucyB1cCB0byBvciBkb3duIHRvIGEgZ2l2ZW4gbWlncmF0aW9uIG5hbWVcbiAgICpcbiAgICogQHBhcmFtIG1pZ3JhdGlvbk5hbWVcbiAgICogQHBhcmFtIGRpcmVjdGlvblxuICAgKi9cbiAgYXN5bmMgcnVuKGRpcmVjdGlvbiA9ICd1cCcsIG1pZ3JhdGlvbk5hbWUpIHtcbiAgICBhd2FpdCB0aGlzLnN5bmMoKTtcblxuICAgIGNvbnN0IHVudGlsTWlncmF0aW9uID0gbWlncmF0aW9uTmFtZSA/XG4gICAgICBhd2FpdCBNaWdyYXRpb25Nb2RlbC5maW5kT25lKHtcbiAgICAgICAgbmFtZTogbWlncmF0aW9uTmFtZVxuICAgICAgfSkgOlxuICAgICAgYXdhaXQgTWlncmF0aW9uTW9kZWwuZmluZE9uZSgpLnNvcnQoe1xuICAgICAgICBjcmVhdGVkQXQ6IC0xXG4gICAgICB9KTtcblxuICAgIGlmICghdW50aWxNaWdyYXRpb24pIHtcbiAgICAgIGlmIChtaWdyYXRpb25OYW1lKSB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJDb3VsZCBub3QgZmluZCB0aGF0IG1pZ3JhdGlvbiBpbiB0aGUgZGF0YWJhc2VcIik7XG4gICAgICBlbHNlIHRocm93IG5ldyBFcnJvcihcIlRoZXJlIGFyZSBubyBwZW5kaW5nIG1pZ3JhdGlvbnMuXCIpO1xuICAgIH1cblxuICAgIGxldCBxdWVyeSA9IHtcbiAgICAgIGNyZWF0ZWRBdDoge1xuICAgICAgICAkbHRlOiB1bnRpbE1pZ3JhdGlvbi5jcmVhdGVkQXRcbiAgICAgIH0sXG4gICAgICBzdGF0ZTogJ2Rvd24nXG4gICAgfTtcblxuICAgIGlmIChkaXJlY3Rpb24gPT0gJ2Rvd24nKSB7XG4gICAgICBxdWVyeSA9IHtcbiAgICAgICAgY3JlYXRlZEF0OiB7XG4gICAgICAgICAgJGd0ZTogdW50aWxNaWdyYXRpb24uY3JlYXRlZEF0XG4gICAgICAgIH0sXG4gICAgICAgIHN0YXRlOiAndXAnXG4gICAgICB9O1xuICAgIH1cblxuXG4gICAgY29uc3Qgc29ydERpcmVjdGlvbiA9IGRpcmVjdGlvbiA9PSAndXAnID8gMSA6IC0xO1xuICAgIGNvbnN0IG1pZ3JhdGlvbnNUb1J1biA9IGF3YWl0IE1pZ3JhdGlvbk1vZGVsLmZpbmQocXVlcnkpXG4gICAgICAuc29ydCh7XG4gICAgICAgIGNyZWF0ZWRBdDogc29ydERpcmVjdGlvblxuICAgICAgfSk7XG5cbiAgICBpZiAoIW1pZ3JhdGlvbnNUb1J1bi5sZW5ndGgpIHtcbiAgICAgIGlmICh0aGlzLmNsaSkge1xuICAgICAgICB0aGlzLmxvZygnVGhlcmUgYXJlIG5vIG1pZ3JhdGlvbnMgdG8gcnVuJy55ZWxsb3cpO1xuICAgICAgICB0aGlzLmxvZyhgQ3VycmVudCBNaWdyYXRpb25zJyBTdGF0dXNlczogYCk7XG4gICAgICAgIGF3YWl0IHRoaXMubGlzdCgpO1xuICAgICAgfVxuICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGVyZSBhcmUgbm8gbWlncmF0aW9ucyB0byBydW4nKTtcbiAgICB9XG5cbiAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgbGV0IG51bU1pZ3JhdGlvbnNSYW4gPSAwO1xuICAgIGxldCBtaWdyYXRpb25zUmFuID0gW107XG5cbiAgICBmb3IgKGNvbnN0IG1pZ3JhdGlvbiBvZiBtaWdyYXRpb25zVG9SdW4pIHtcbiAgICAgIGNvbnN0IG1pZ3JhdGlvbkZpbGVQYXRoID0gcGF0aC5qb2luKHNlbGYubWlncmF0aW9uUGF0aCwgbWlncmF0aW9uLmZpbGVuYW1lKTtcbiAgICAgIGlmICh0aGlzLmVzNikge1xuICAgICAgICByZXF1aXJlKCdiYWJlbC1yZWdpc3RlcicpKHtcbiAgICAgICAgICBcInByZXNldHNcIjogW3JlcXVpcmUoXCJiYWJlbC1wcmVzZXQtZW52XCIpLCByZXF1aXJlKFwiYmFiZWwtcHJlc2V0LXN0YWdlLTNcIildLFxuICAgICAgICAgIFwicGx1Z2luc1wiOiBbcmVxdWlyZShcImJhYmVsLXBsdWdpbi10cmFuc2Zvcm0tcnVudGltZVwiKSwgcmVxdWlyZShcImJhYmVsLXBsdWdpbi1hZGQtbW9kdWxlLWV4cG9ydHNcIildXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJlcXVpcmUoJ2JhYmVsLXBvbHlmaWxsJyk7XG4gICAgICB9XG5cbiAgICAgIGxldCBtaWdyYXRpb25GdW5jdGlvbnM7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIG1pZ3JhdGlvbkZ1bmN0aW9ucyA9IHJlcXVpcmUobWlncmF0aW9uRmlsZVBhdGgpO1xuICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIGVyci5tZXNzYWdlID0gZXJyLm1lc3NhZ2UgJiYgL1VuZXhwZWN0ZWQgdG9rZW4vLnRlc3QoZXJyLm1lc3NhZ2UpID9cbiAgICAgICAgICAnVW5leHBlY3RlZCBUb2tlbiB3aGVuIHBhcnNpbmcgbWlncmF0aW9uLiBJZiB5b3UgYXJlIHVzaW5nIGFuIEVTNiBtaWdyYXRpb24gZmlsZSwgdXNlIG9wdGlvbiAtLWVzNicgOlxuICAgICAgICAgIGVyci5tZXNzYWdlO1xuICAgICAgICB0aHJvdyBlcnI7XG4gICAgICB9XG5cbiAgICAgIGlmICghbWlncmF0aW9uRnVuY3Rpb25zW2RpcmVjdGlvbl0pIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBUaGUgJHtkaXJlY3Rpb259IGV4cG9ydCBpcyBub3QgZGVmaW5lZCBpbiAke21pZ3JhdGlvbi5maWxlbmFtZX0uYC5yZWQpO1xuICAgICAgfVxuXG4gICAgICB0cnkge1xuICAgICAgICBhd2FpdCBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgY29uc3QgY2FsbFByb21pc2UgPSBtaWdyYXRpb25GdW5jdGlvbnNbZGlyZWN0aW9uXS5jYWxsKFxuICAgICAgICAgICAgdGhpcy5jb25uZWN0aW9uLm1vZGVsLmJpbmQodGhpcy5jb25uZWN0aW9uKSxcbiAgICAgICAgICAgIGZ1bmN0aW9uIGNhbGxiYWNrKGVycikge1xuICAgICAgICAgICAgICBpZiAoZXJyKSByZXR1cm4gcmVqZWN0KGVycik7XG4gICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICApO1xuXG4gICAgICAgICAgaWYgKGNhbGxQcm9taXNlICYmIHR5cGVvZiBjYWxsUHJvbWlzZS50aGVuID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICBjYWxsUHJvbWlzZS50aGVuKHJlc29sdmUpLmNhdGNoKHJlamVjdCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmxvZyhgJHtkaXJlY3Rpb24udG9VcHBlckNhc2UoKX06ICAgYCBbZGlyZWN0aW9uID09ICd1cCcgPyAnZ3JlZW4nIDogJ3JlZCddICsgYCAke21pZ3JhdGlvbi5maWxlbmFtZX0gYCk7XG5cbiAgICAgICAgYXdhaXQgTWlncmF0aW9uTW9kZWwud2hlcmUoe1xuICAgICAgICAgIG5hbWU6IG1pZ3JhdGlvbi5uYW1lXG4gICAgICAgIH0pLnVwZGF0ZU1hbnkoe1xuICAgICAgICAgICRzZXQ6IHtcbiAgICAgICAgICAgIHN0YXRlOiBkaXJlY3Rpb25cbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBtaWdyYXRpb25zUmFuLnB1c2gobWlncmF0aW9uLnRvSlNPTigpKTtcbiAgICAgICAgbnVtTWlncmF0aW9uc1JhbisrO1xuICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIHRoaXMubG9nKGBGYWlsZWQgdG8gcnVuIG1pZ3JhdGlvbiAke21pZ3JhdGlvbi5uYW1lfSBkdWUgdG8gYW4gZXJyb3IuYC5yZWQpO1xuICAgICAgICB0aGlzLmxvZyhgTm90IGNvbnRpbnVpbmcuIE1ha2Ugc3VyZSB5b3VyIGRhdGEgaXMgaW4gY29uc2lzdGVudCBzdGF0ZWAucmVkKTtcbiAgICAgICAgdGhyb3cgZXJyIGluc3RhbmNlb2YoRXJyb3IpID8gZXJyIDogbmV3IEVycm9yKGVycik7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKG1pZ3JhdGlvbnNUb1J1bi5sZW5ndGggPT0gbnVtTWlncmF0aW9uc1JhbikgdGhpcy5sb2coJ0FsbCBtaWdyYXRpb25zIGZpbmlzaGVkIHN1Y2Nlc3NmdWxseS4nLmdyZWVuKTtcbiAgICByZXR1cm4gbWlncmF0aW9uc1JhbjtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb29rcyBhdCB0aGUgZmlsZSBzeXN0ZW0gbWlncmF0aW9ucyBhbmQgaW1wb3J0cyBhbnkgbWlncmF0aW9ucyB0aGF0IGFyZVxuICAgKiBvbiB0aGUgZmlsZSBzeXN0ZW0gYnV0IG1pc3NpbmcgaW4gdGhlIGRhdGFiYXNlIGludG8gdGhlIGRhdGFiYXNlXG4gICAqXG4gICAqIFRoaXMgZnVuY3Rpb25hbGl0eSBpcyBvcHBvc2l0ZSBvZiBwcnVuZSgpXG4gICAqL1xuICBhc3luYyBzeW5jKCkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCBmaWxlc0luTWlncmF0aW9uRm9sZGVyID0gZnMucmVhZGRpclN5bmModGhpcy5taWdyYXRpb25QYXRoKTtcbiAgICAgIGNvbnN0IG1pZ3JhdGlvbnNJbkRhdGFiYXNlID0gYXdhaXQgTWlncmF0aW9uTW9kZWwuZmluZCh7fSk7XG4gICAgICAvLyBHbyBvdmVyIG1pZ3JhdGlvbnMgaW4gZm9sZGVyIGFuZCBkZWxldGUgYW55IGZpbGVzIG5vdCBpbiBEQlxuICAgICAgY29uc3QgbWlncmF0aW9uc0luRm9sZGVyID0gXy5maWx0ZXIoZmlsZXNJbk1pZ3JhdGlvbkZvbGRlciwgZmlsZSA9PiAvXFxkezEzLH1cXC0uKy5qcyQvLnRlc3QoZmlsZSkpXG4gICAgICAgIC5tYXAoZmlsZW5hbWUgPT4ge1xuICAgICAgICAgIGNvbnN0IGZpbGVDcmVhdGVkQXQgPSBwYXJzZUludChmaWxlbmFtZS5zcGxpdCgnLScpWzBdKTtcbiAgICAgICAgICBjb25zdCBleGlzdHNJbkRhdGFiYXNlID0gbWlncmF0aW9uc0luRGF0YWJhc2Uuc29tZShtID0+IGZpbGVuYW1lID09IG0uZmlsZW5hbWUpO1xuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjcmVhdGVkQXQ6IGZpbGVDcmVhdGVkQXQsXG4gICAgICAgICAgICBmaWxlbmFtZSxcbiAgICAgICAgICAgIGV4aXN0c0luRGF0YWJhc2VcbiAgICAgICAgICB9O1xuICAgICAgICB9KTtcblxuICAgICAgY29uc3QgZmlsZXNOb3RJbkRiID0gXy5maWx0ZXIobWlncmF0aW9uc0luRm9sZGVyLCB7XG4gICAgICAgIGV4aXN0c0luRGF0YWJhc2U6IGZhbHNlXG4gICAgICB9KS5tYXAoZiA9PiBmLmZpbGVuYW1lKTtcbiAgICAgIGxldCBtaWdyYXRpb25zVG9JbXBvcnQgPSBmaWxlc05vdEluRGI7XG4gICAgICB0aGlzLmxvZygnU3luY2hyb25pemluZyBkYXRhYmFzZSB3aXRoIGZpbGUgc3lzdGVtIG1pZ3JhdGlvbnMuLi4nKTtcbiAgICAgIGlmICghdGhpcy5hdXRvc3luYyAmJiBtaWdyYXRpb25zVG9JbXBvcnQubGVuZ3RoKSB7XG4gICAgICAgIGNvbnN0IGFuc3dlcnMgPSBhd2FpdCBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlKSB7XG4gICAgICAgICAgYXNrLnByb21wdCh7XG4gICAgICAgICAgICB0eXBlOiAnY2hlY2tib3gnLFxuICAgICAgICAgICAgbWVzc2FnZTogJ1RoZSBmb2xsb3dpbmcgbWlncmF0aW9ucyBleGlzdCBpbiB0aGUgbWlncmF0aW9ucyBmb2xkZXIgYnV0IG5vdCBpbiB0aGUgZGF0YWJhc2UuIFNlbGVjdCB0aGUgb25lcyB5b3Ugd2FudCB0byBpbXBvcnQgaW50byB0aGUgZGF0YWJhc2UnLFxuICAgICAgICAgICAgbmFtZTogJ21pZ3JhdGlvbnNUb0ltcG9ydCcsXG4gICAgICAgICAgICBjaG9pY2VzOiBmaWxlc05vdEluRGJcbiAgICAgICAgICB9LCAoYW5zd2VycykgPT4ge1xuICAgICAgICAgICAgcmVzb2x2ZShhbnN3ZXJzKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgbWlncmF0aW9uc1RvSW1wb3J0ID0gYW5zd2Vycy5taWdyYXRpb25zVG9JbXBvcnQ7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBQcm9taXNlLm1hcChtaWdyYXRpb25zVG9JbXBvcnQsIGFzeW5jIChtaWdyYXRpb25Ub0ltcG9ydCkgPT4ge1xuICAgICAgICBjb25zdCBmaWxlUGF0aCA9IHBhdGguam9pbih0aGlzLm1pZ3JhdGlvblBhdGgsIG1pZ3JhdGlvblRvSW1wb3J0KSxcbiAgICAgICAgICB0aW1lc3RhbXBTZXBhcmF0b3JJbmRleCA9IG1pZ3JhdGlvblRvSW1wb3J0LmluZGV4T2YoJy0nKSxcbiAgICAgICAgICB0aW1lc3RhbXAgPSBtaWdyYXRpb25Ub0ltcG9ydC5zbGljZSgwLCB0aW1lc3RhbXBTZXBhcmF0b3JJbmRleCksXG4gICAgICAgICAgbWlncmF0aW9uTmFtZSA9IG1pZ3JhdGlvblRvSW1wb3J0LnNsaWNlKHRpbWVzdGFtcFNlcGFyYXRvckluZGV4ICsgMSwgbWlncmF0aW9uVG9JbXBvcnQubGFzdEluZGV4T2YoJy4nKSk7XG5cbiAgICAgICAgdGhpcy5sb2coYEFkZGluZyBtaWdyYXRpb24gJHtmaWxlUGF0aH0gaW50byBkYXRhYmFzZSBmcm9tIGZpbGUgc3lzdGVtLiBTdGF0ZSBpcyBgICsgYERPV05gLnJlZCk7XG4gICAgICAgIGNvbnN0IGNyZWF0ZWRNaWdyYXRpb24gPSBhd2FpdCBNaWdyYXRpb25Nb2RlbC5jcmVhdGUoe1xuICAgICAgICAgIG5hbWU6IG1pZ3JhdGlvbk5hbWUsXG4gICAgICAgICAgY3JlYXRlZEF0OiB0aW1lc3RhbXBcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBjcmVhdGVkTWlncmF0aW9uLnRvSlNPTigpO1xuICAgICAgfSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIHRoaXMubG9nKGBDb3VsZCBub3Qgc3luY2hyb25pc2UgbWlncmF0aW9ucyBpbiB0aGUgbWlncmF0aW9ucyBmb2xkZXIgdXAgdG8gdGhlIGRhdGFiYXNlLmAucmVkKTtcbiAgICAgIHRocm93IGVycm9yO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBPcHBvc2l0ZSBvZiBzeW5jKCkuXG4gICAqIFJlbW92ZXMgZmlsZXMgaW4gbWlncmF0aW9uIGRpcmVjdG9yeSB3aGljaCBkb24ndCBleGlzdCBpbiBkYXRhYmFzZS5cbiAgICovXG4gIGFzeW5jIHBydW5lKCkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCBmaWxlc0luTWlncmF0aW9uRm9sZGVyID0gZnMucmVhZGRpclN5bmModGhpcy5taWdyYXRpb25QYXRoKTtcbiAgICAgIGNvbnN0IG1pZ3JhdGlvbnNJbkRhdGFiYXNlID0gYXdhaXQgTWlncmF0aW9uTW9kZWwuZmluZCh7fSk7XG4gICAgICAvLyBHbyBvdmVyIG1pZ3JhdGlvbnMgaW4gZm9sZGVyIGFuZCBkZWxldGUgYW55IGZpbGVzIG5vdCBpbiBEQlxuICAgICAgY29uc3QgbWlncmF0aW9uc0luRm9sZGVyID0gXy5maWx0ZXIoZmlsZXNJbk1pZ3JhdGlvbkZvbGRlciwgZmlsZSA9PiAvXFxkezEzLH1cXC0uKy5qcy8udGVzdChmaWxlKSlcbiAgICAgICAgLm1hcChmaWxlbmFtZSA9PiB7XG4gICAgICAgICAgY29uc3QgZmlsZUNyZWF0ZWRBdCA9IHBhcnNlSW50KGZpbGVuYW1lLnNwbGl0KCctJylbMF0pO1xuICAgICAgICAgIGNvbnN0IGV4aXN0c0luRGF0YWJhc2UgPSBtaWdyYXRpb25zSW5EYXRhYmFzZS5zb21lKG0gPT4gZmlsZW5hbWUgPT0gbS5maWxlbmFtZSk7XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGNyZWF0ZWRBdDogZmlsZUNyZWF0ZWRBdCxcbiAgICAgICAgICAgIGZpbGVuYW1lLFxuICAgICAgICAgICAgZXhpc3RzSW5EYXRhYmFzZVxuICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuXG4gICAgICBjb25zdCBkYk1pZ3JhdGlvbnNOb3RPbkZzID0gXy5maWx0ZXIobWlncmF0aW9uc0luRGF0YWJhc2UsIG0gPT4ge1xuICAgICAgICByZXR1cm4gIV8uZmluZChtaWdyYXRpb25zSW5Gb2xkZXIsIHtcbiAgICAgICAgICBmaWxlbmFtZTogbS5maWxlbmFtZVxuICAgICAgICB9KVxuICAgICAgfSk7XG5cblxuICAgICAgbGV0IG1pZ3JhdGlvbnNUb0RlbGV0ZSA9IGRiTWlncmF0aW9uc05vdE9uRnMubWFwKG0gPT4gbS5uYW1lKTtcblxuICAgICAgaWYgKCF0aGlzLmF1dG9zeW5jICYmICEhbWlncmF0aW9uc1RvRGVsZXRlLmxlbmd0aCkge1xuICAgICAgICBjb25zdCBhbnN3ZXJzID0gYXdhaXQgbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSkge1xuICAgICAgICAgIGFzay5wcm9tcHQoe1xuICAgICAgICAgICAgdHlwZTogJ2NoZWNrYm94JyxcbiAgICAgICAgICAgIG1lc3NhZ2U6ICdUaGUgZm9sbG93aW5nIG1pZ3JhdGlvbnMgZXhpc3QgaW4gdGhlIGRhdGFiYXNlIGJ1dCBub3QgaW4gdGhlIG1pZ3JhdGlvbnMgZm9sZGVyLiBTZWxlY3QgdGhlIG9uZXMgeW91IHdhbnQgdG8gcmVtb3ZlIGZyb20gdGhlIGZpbGUgc3lzdGVtLicsXG4gICAgICAgICAgICBuYW1lOiAnbWlncmF0aW9uc1RvRGVsZXRlJyxcbiAgICAgICAgICAgIGNob2ljZXM6IG1pZ3JhdGlvbnNUb0RlbGV0ZVxuICAgICAgICAgIH0sIChhbnN3ZXJzKSA9PiB7XG4gICAgICAgICAgICByZXNvbHZlKGFuc3dlcnMpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBtaWdyYXRpb25zVG9EZWxldGUgPSBhbnN3ZXJzLm1pZ3JhdGlvbnNUb0RlbGV0ZTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgbWlncmF0aW9uc1RvRGVsZXRlRG9jcyA9IGF3YWl0IE1pZ3JhdGlvbk1vZGVsXG4gICAgICAgIC5maW5kKHtcbiAgICAgICAgICBuYW1lOiB7XG4gICAgICAgICAgICAkaW46IG1pZ3JhdGlvbnNUb0RlbGV0ZVxuICAgICAgICAgIH1cbiAgICAgICAgfSkubGVhbigpO1xuXG4gICAgICBpZiAobWlncmF0aW9uc1RvRGVsZXRlLmxlbmd0aCkge1xuICAgICAgICB0aGlzLmxvZyhgUmVtb3ZpbmcgbWlncmF0aW9uKHMpIGAsIGAke21pZ3JhdGlvbnNUb0RlbGV0ZS5qb2luKCcsICcpfWAuY3lhbiwgYCBmcm9tIGRhdGFiYXNlYCk7XG4gICAgICAgIGF3YWl0IE1pZ3JhdGlvbk1vZGVsLnJlbW92ZSh7XG4gICAgICAgICAgbmFtZToge1xuICAgICAgICAgICAgJGluOiBtaWdyYXRpb25zVG9EZWxldGVcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbWlncmF0aW9uc1RvRGVsZXRlRG9jcztcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgdGhpcy5sb2coYENvdWxkIG5vdCBwcnVuZSBleHRyYW5lb3VzIG1pZ3JhdGlvbnMgZnJvbSBkYXRhYmFzZS5gLnJlZCk7XG4gICAgICB0aHJvdyBlcnJvcjtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTGlzdHMgdGhlIGN1cnJlbnQgbWlncmF0aW9ucyBhbmQgdGhlaXIgc3RhdHVzZXNcbiAgICogQHJldHVybnMge1Byb21pc2U8QXJyYXk8T2JqZWN0Pj59XG4gICAqIEBleGFtcGxlXG4gICAqICAgW1xuICAgKiAgICB7IG5hbWU6ICdteS1taWdyYXRpb24nLCBmaWxlbmFtZTogJzE0OTIxMzIyMzQyNF9teS1taWdyYXRpb24uanMnLCBzdGF0ZTogJ3VwJyB9LFxuICAgKiAgICB7IG5hbWU6ICdhZGQtY293cycsIGZpbGVuYW1lOiAnMTQ5MjEzMjIzNDUzX2FkZC1jb3dzLmpzJywgc3RhdGU6ICdkb3duJyB9XG4gICAqICAgXVxuICAgKi9cbiAgYXN5bmMgbGlzdCgpIHtcbiAgICBhd2FpdCB0aGlzLnN5bmMoKTtcbiAgICBjb25zdCBtaWdyYXRpb25zID0gYXdhaXQgTWlncmF0aW9uTW9kZWwuZmluZCgpLnNvcnQoe1xuICAgICAgY3JlYXRlZEF0OiAxXG4gICAgfSk7XG4gICAgaWYgKCFtaWdyYXRpb25zLmxlbmd0aCkgdGhpcy5sb2coJ1RoZXJlIGFyZSBubyBtaWdyYXRpb25zIHRvIGxpc3QuJy55ZWxsb3cpO1xuICAgIHJldHVybiBtaWdyYXRpb25zLm1hcCgobSkgPT4ge1xuICAgICAgdGhpcy5sb2coXG4gICAgICAgIGAke20uc3RhdGUgPT0gJ3VwJyA/ICdVUDogIFxcdCcgOiAnRE9XTjpcXHQnfWAgW20uc3RhdGUgPT0gJ3VwJyA/ICdncmVlbicgOiAncmVkJ10gK1xuICAgICAgICBgICR7bS5maWxlbmFtZX1gXG4gICAgICApO1xuICAgICAgcmV0dXJuIG0udG9KU09OKCk7XG4gICAgfSk7XG4gIH1cbn1cblxuXG5cbmZ1bmN0aW9uIGZpbGVSZXF1aXJlZChlcnJvcikge1xuICBpZiAoZXJyb3IgJiYgZXJyb3IuY29kZSA9PSAnRU5PRU5UJykge1xuICAgIHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihgQ291bGQgbm90IGZpbmQgYW55IGZpbGVzIGF0IHBhdGggJyR7ZXJyb3IucGF0aH0nYCk7XG4gIH1cbn1cblxuXG5tb2R1bGUuZXhwb3J0cyA9IE1pZ3JhdG9yO1xuIl19